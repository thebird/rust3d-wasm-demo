const rust = import('./pkg/rust3d');
const canvas = document.getElementById('rustCanvas');
const gl = canvas.getContext('webgl', {antialias: true});

rust.then(m => {
    if(!gl) {
        alert('Failed to initialize WebGL');
        return;
    }

    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

        console.log('what')

    //Think about splitting the update/render fns
    //to allow for updating more often than rendering
    const FPS = 1000.0 / 30.0; 
    let lastDrawTime = -1;
    const initialTime = Date.now();
    const rsClient = new m.RsClient();

    function render() {
        window.requestAnimationFrame(render);
        const currTime = Date.now();

        if (currTime >= lastDrawTime + FPS) {
            lastDrawTime = currTime;
            //Rust update call
            //Rust render call
        }

        if (window.innerHeight != canvas.height ||
            window.innerWidth != canvas.width) {
            canvas.height = window.innerHeight;
            canvas.clientHeight = window.innerHeight;
            canvas.style.height = window.innerHeight;

            canvas.width = window.innerWidth;
            canvas.clientWidth = window.innerWidth;
            canvas.style.width = window.innerWidth

            gl.viewport(0, 0, window.innerWidth, window.innerHeight);
        }

        let elapsedTime = currTime - initialTime;
        rsClient.update(elapsedTime, window.innerHeight, window.innerWidth);
        rsClient.render();
    }
    render();
});