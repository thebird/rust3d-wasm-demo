extern crate wasm_bindgen;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
//Grab something external
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
pub struct RsClient {

}

#[wasm_bindgen]
impl RsClient {

    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        log("Here");
        Self {

        }
    }

    pub fn update(&mut self, time: f32, height: f32, width: f32) -> Result<(), JsValue> {
        Ok(())
    }

    pub fn render(&self) {

    }
}